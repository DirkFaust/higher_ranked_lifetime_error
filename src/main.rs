use std::collections::HashSet;

use futures::{stream::FuturesUnordered, StreamExt};

struct Foo {}

impl Foo {
    pub async fn remove_from(&mut self, other: impl IntoIterator<Item = &String>) {
        for _ in other {
            async {}.await;
        }
    }
}

pub async fn some_task_higher_ranked_lifetime_error(mut foo: Foo) -> Result<(), ()> {
    loop {
        tokio::select! {
            _ = tokio::time::sleep(tokio::time::Duration::from_millis(100)) => {
                let blah: HashSet<String> = HashSet::new();
                let blub: HashSet<String> = HashSet::new();

                // This does not work
                foo.remove_from(blah.iter().chain(blub.iter())).await;

                // This works :shrug:
                // foo.remove_from(blah.iter()).await;
                // foo.remove_from(blub.iter()).await;
            }
        }
    }
}

#[tokio::main]
async fn main() {
    let foo = Foo {};

    let mut futs = FuturesUnordered::new();
    futs.push(tokio::spawn(some_task_higher_ranked_lifetime_error(foo)));
    futs.next().await;
}
